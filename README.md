# bin2c

Reads a binary file on stdin and produce C code on stdout.
The C code consists of one array containing the binary content of the file.

## Arguments

```
bin2c [array_name]
```

array_name specifies the name of the array. By default it is binary_array.

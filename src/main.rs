use std::{io::{self, Read}, env};

fn main() -> io::Result<()> {
    let args: Vec<String> = env::args().collect();
    let name = if args.len() == 2 {
        &args[1]
    } else {
        "binary_array"
    };
    println!("#include <stdint.h>");
    println!("uint8_t {}[] = {{", name);
    print!("   ");
    for b in io::stdin().bytes() {
        print!(" {},", b?);
    }
    println!("\n}};");

    Ok(())
}
